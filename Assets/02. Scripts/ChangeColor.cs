﻿using UnityEngine;

public class ChangeColor : MonoBehaviour
{

    public GameObject[] lights;
    public float MinEnabledTime = 1.0f;
    public float MaxEnabledTime = 3.0f;

    private GameObject lightEnabled;

	// Use this for initialization
	void Start ()
	{
	    if (lights.Length == 0)
	    {
	        Debug.LogError("No lights attached");
	    }
	    else
	    {
            lightEnabled = lights[Random.Range(0, lights.Length - 1)];
            Debug.Log("Encendemos primera luz: " + lightEnabled.name);
	        ToggleLight();
	    }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    /// <summary>
    /// Disables enabled light, slects another random one an enables it a random amount of time
    /// </summary>
	private void ToggleLight(){
		lightEnabled.SetActive(false);
        lightEnabled = lights[Random.Range(0, lights.Length - 1)];
        Debug.Log("Encendemos luz: " + lightEnabled.name);
        lightEnabled.SetActive(true);
	    float timeEnabled = Random.Range(MinEnabledTime, MaxEnabledTime);
        Invoke("ToggleLight", timeEnabled);
	}
}
