﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	public string SceneName;
	public Button btn;

	// Use this for initialization
	void Start () {
		btn.onClick.AddListener (GotToScene);
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void GotToScene(){
		SceneManager.LoadScene (SceneName);
	}
}
